package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.token.NumberToken;
import com.tsystems.javaschool.tasks.calculator.token.Token;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TokenizerTest {

    private Tokenizer tokenizer = new Tokenizer();

    @Test
    public void testTokenize() throws Exception {
        //given
        String input = "(2+3.5)*5";
        List<Token> expected = new ArrayList<>();
        expected.add(new Token("("));
        expected.add(new NumberToken("2", 2.0));
        expected.add(new Token("+"));
        expected.add(new NumberToken("3.5", 3.5));
        expected.add(new Token(")"));
        expected.add(new Token("*"));
        expected.add(new NumberToken("5", 5.0));
        System.out.println(expected);

        //run
        List<Token> result = tokenizer.tokenize(input);
        System.out.println(result);

        //assert
        Assert.assertTrue(tokenListsAreEqual(expected, result));
    }

    private boolean tokenListsAreEqual(List<Token> expected, List<Token> result) {
        if (expected.size() != result.size()) return false;

        for (int i = 0; i < expected.size(); i++) {
            Token token1 = expected.get(i),
                    token2 = result.get(i);
            if (! token1.equals(token2)) return false;
        }
        return true;
    }
}
