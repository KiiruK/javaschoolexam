package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int seqLength = inputNumbers.size();
        double pyrDepth = (Math.sqrt(1.0 + 8.0 * seqLength) - 1.0) / 2.0;

        if (Double.compare(pyrDepth, Math.rint(pyrDepth)) != 0) {
            throw new CannotBuildPyramidException();
        }

        int n = (int) Math.rint(pyrDepth);
        int m = 2 * n - 1;

        try {
            inputNumbers.sort(Comparator.naturalOrder());
        }
        catch (Exception e) {
            throw new CannotBuildPyramidException();
        }

        Iterator<Integer> iterator = inputNumbers.iterator();

        int[][] matrix = new int[n][m];
        for (int i = 0; i < n; i++) {
            int start = n - i - 1, end = n + i;

            for (int j = 0; j < start; j++)
                matrix[i][j] = 0;

            for (int j = start; j < end; j += 2) {
                if (iterator.hasNext()) {
                    matrix[i][j] = iterator.next();
                }
                if (j + 1 < end)
                    matrix[i][j + 1] = 0;
            }

            for (int j = end; j < m; j++)
                matrix[i][j] = 0;
        }
        return matrix;
    }
}
