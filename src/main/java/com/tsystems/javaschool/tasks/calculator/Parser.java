package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.CannotParseException;
import com.tsystems.javaschool.tasks.calculator.token.*;

import java.util.List;
import java.util.ListIterator;

public class Parser {

    /**
     * Parses list of tokens into expression
     *
     * @param tokens list of tokens representing numbers, parentheses and operations signs
     * @return Double value of expression
     * @throws CannotParseException if there are errors in the sequence of tokens
     */
    public Double parse(List<Token> tokens) throws CannotParseException {
        ListIterator<Token> iterator = tokens.listIterator();
        Double expression = parseExpression(iterator);

        // the statement should be parsed fully
        if (iterator.hasNext()) throw new CannotParseException();

        return expression;
    }

    private Double parseExpression(ListIterator<Token> iterator) throws CannotParseException {
        if (! iterator.hasNext()) throw new CannotParseException();

        Double left = parseTerm(iterator);
        Token token;
        if (iterator.hasNext())
            token = iterator.next();
        else
            return left;

        while (true) {
            switch (token.getValue()) {
                case "+":
                    left += parseTerm(iterator);
                    if (iterator.hasNext())
                        token = iterator.next();
                    else
                        return left;
                    break;

                case "-":
                    left -= parseTerm(iterator);
                    if (iterator.hasNext())
                        token = iterator.next();
                    else
                        return left;
                    break;

                default:
                    iterator.previous();
                    return left;
            }
        }
    }

    private Double parseTerm(ListIterator<Token> iterator) throws CannotParseException {
        Double left = parsePrimary(iterator);
        Token token;
        if (iterator.hasNext())
            token = iterator.next();
        else
            return left;

        while (true) {
            switch (token.getValue()) {
                case "*":
                    left *= parsePrimary(iterator);
                    if (iterator.hasNext())
                        token = iterator.next();
                    else
                        return left;
                    break;

                case "/":
                    Double right = parsePrimary(iterator);
                    if (right.equals(0.0)) throw new CannotParseException();
                    left /= right;
                    if (iterator.hasNext())
                        token = iterator.next();
                    else
                        return left;
                    break;

                default:
                    iterator.previous();
                    return left;
            }
        }
    }

    private Double parsePrimary(ListIterator<Token> iterator) throws CannotParseException {

        Token token = iterator.next();
        if (token instanceof NumberToken) {
            return parseNumber(token);
        } else {
            switch (token.getValue()) {
                case "(":
                    return parseParentheses(iterator);

                default:
                    throw new CannotParseException();
            }
        }
    }

    private Double parseNumber(Token token) {
        return Double.parseDouble(token.getValue());
    }

    private Double parseParentheses(ListIterator<Token> iterator) throws CannotParseException {
        Double expression = parseExpression(iterator);
        if (! iterator.hasNext() || !")".equals(iterator.next().getValue()))
            throw new CannotParseException();
        return expression;
    }
}
