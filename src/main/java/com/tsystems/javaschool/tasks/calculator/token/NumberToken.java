package com.tsystems.javaschool.tasks.calculator.token;

public class NumberToken extends Token {

    private Double number;

    public NumberToken(String sNumber, Double dNumber) {
        super(sNumber);
        this.number = dNumber;
    }

    public Double getNumber() {
        return number;
    }
}
