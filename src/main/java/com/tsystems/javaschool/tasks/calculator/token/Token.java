package com.tsystems.javaschool.tasks.calculator.token;

public class Token {

    protected String value;

    public Token(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        return value != null ? value.equals(token.value) : token.value == null;

    }
}
