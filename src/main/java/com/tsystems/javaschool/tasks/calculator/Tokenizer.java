package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.CannotTokenizeException;
import com.tsystems.javaschool.tasks.calculator.token.*;

import java.util.ArrayList;
import java.util.List;

public class Tokenizer {

    /**
     * Tokenizes statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'
     * @return list of tokens
     * @throws CannotTokenizeException if statement is invalid
     */
    public List<Token> tokenize(String statement) throws CannotTokenizeException {

        List<Token> tokens = new ArrayList<>();

        int index = 0;
        char current;
        StringBuilder sNumber = new StringBuilder();
        Double dNumber = 0.0;

        while (index < statement.length()) {

            current = statement.charAt(index);

            // meet a number --> accumulate digits
            if (isNumber(current) || isDot(current)) {
                sNumber.append(current);
            } else {

                if (sNumber.length() > 0) {
                    // digits are no more --> eval the number
                    if (tryParseNumber(sNumber.toString(), dNumber))
                        tokens.add(new NumberToken(sNumber.toString(), dNumber));
                    else
                        throw new CannotTokenizeException();
                    sNumber = new StringBuilder();
                }

                switch (current) {
                    case '(':
                        tokens.add(new Token("("));
                        break;
                    case ')':
                        tokens.add(new Token(")"));
                        break;
                    case '*':
                        tokens.add(new Token("*"));
                        break;
                    case '/':
                        tokens.add(new Token("/"));
                        break;
                    case '+':
                        tokens.add(new Token("+"));
                        break;
                    case '-':
                        tokens.add(new Token("-"));
                        break;
                    default:
                        // wrong lex
                        return new ArrayList<>();
                }
            }
            index++;
        }
        // check digit buffer one last time
        if (sNumber.length() > 0) {
            if (tryParseNumber(sNumber.toString(), dNumber))
                tokens.add(new NumberToken(sNumber.toString(), dNumber));
            else
                throw new CannotTokenizeException();
        }
        return tokens;
    }

    private boolean isNumber(char symbol) {
        return (symbol >= '0' && symbol <= '9');
    }

    private boolean isDot(char symbol) {
        return symbol == '.';
    }

    private boolean tryParseNumber(String sNumber, Double dNumber) {
        try {
            dNumber = Double.parseDouble(sNumber);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
