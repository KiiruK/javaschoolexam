package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.token.Token;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        Tokenizer tokenizer = new Tokenizer();
        List<Token> tokens;
        try {
            tokens = tokenizer.tokenize(statement);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Parser parser = new Parser();
        Double result;
        try {
            result = parser.parse(tokens);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        NumberFormat nf = new DecimalFormat("#.####");
        return nf.format(result).replace(',', '.');
    }

}
